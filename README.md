# Test task application

## Диаграмма класса
![img.png](docks/img.png)
## Описание
Данное приложение разработано с использованием Spring Boot 3.1.3, Java 17, Docker, Docker Compose, Hibernate и PostgreSQL. Приложение позволяет создавать пользователей, получать информацию о пользователе по его идентификатору и изменять его статус (онлайн или офлайн). Также в приложении реализована обработка ошибок и вся документация доступна через Swagger.

## Установка и запуск
1. Установите Docker и Docker Compose на вашу систему, если они еще не установлены.
2. Склонируйте репозиторий с приложением.
3. Перейдите в корневую директорию проекта.
4. Создайте файл `.env` и укажите следующие переменные окружения:
   - HOST=${HOST}
   - POSTGRES_USERNAME=${USERNAME}
   - POSTGRES_PASSWORD=${DB_PASSWORD}
   - POSTGRES_DATABASE=${DB_NAME}

5. Запустите Docker Compose, чтобы создать и запустить контейнеры с PostgreSQL и приложением. Выполните команду `docker-compose up -d`.
6. После успешного запуска контейнеров, приложение будет доступно по адресу `http://localhost:8080`.

## Использование
Документация API доступна через Swagger по адресу `http://localhost:8080/swagger-ui.html`. Здесь вы можете ознакомиться со всеми доступными методами и их параметрами.

### Создание пользователя
URL: `POST http://localhost:8080/api/v1/user`

Для создания пользователя отправьте POST запрос на указанный URL с данными пользователя в формате JSON в теле запроса. Пример запроса:

```json
{
  "name": "{{$randomUserName}}",
  "surName": "{{$randomUserName}}",
  "email": "{{$randomEmail}}",
  "imageUrl": "http://",
  "status": "online",
  "registrationDate": "",
  "birthDate": "",
  "role": "ADMIN"
}
```

### Получение пользователя по ID
URL: `GET http://localhost:8080/api/v1/user/{Id}`

Для получения информации о пользователе по его идентификатору, отправьте GET запрос на указанный URL, заменив `{id}` на фактический идентификатор пользователя.

### Изменение статуса пользователя
URL: `PUT /users/{id}`

Для изменения статуса пользователя (онлайн или офлайн), отправьте PUT запрос на указанный URL, заменив `{id}` на фактический идентификатор пользователя.


## Обработка ошибок
Приложение реализует обработку различных ошибок. Если происходит ошибка, вы получите соответствующий HTTP статус код и сообщение об ошибке в формате JSON.

## Заключение
Приложение позволяет создавать пользователей, получать информацию о пользователе по его идентификатору и изменять его статус. Swagger предоставляет детальную документацию по всем методам.