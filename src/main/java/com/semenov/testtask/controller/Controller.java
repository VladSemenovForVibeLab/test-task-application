package com.semenov.testtask.controller;


import com.semenov.testtask.model.Users;
import com.semenov.testtask.response.ResponseHandler;
import com.semenov.testtask.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
@Tag(name = "Users Controller",description = "Users API")
public class Controller {
    UserService userService;
    @GetMapping("/{userId}")
    @Operation(summary = "Get User")
    public ResponseEntity<Object> getUser(@PathVariable String userId){
        return userService.findById(Long.parseLong(userId));
    }
    @PostMapping
    @Operation(summary = "Add User")
    public ResponseEntity<Object> addUser(@RequestBody Users user){
        return userService.save(user);
    }
    @PutMapping("/{userId}")
    @Operation(summary = "Put offline -> online")
    public ResponseEntity<Object> updateUserStatus(@PathVariable String userId){
        return userService.updateUser(Long.parseLong(userId));
    }
}
