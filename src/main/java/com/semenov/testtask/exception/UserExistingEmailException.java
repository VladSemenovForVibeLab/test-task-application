package com.semenov.testtask.exception;

import org.springframework.web.bind.annotation.RestControllerAdvice;


public class UserExistingEmailException extends RuntimeException{
    public UserExistingEmailException(String message){
        super(message);
    }
    public UserExistingEmailException(String message,Throwable cause){
        super(message,cause);
    }
}
