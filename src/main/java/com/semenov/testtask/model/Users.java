package com.semenov.testtask.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "User Id",example = "1")
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @Column(name = "name", nullable = false)
    @Schema(description = "Name user",example = "Vlad")
    private String name;
    @Column(name = "surName", nullable = false)
    @Schema(description = "Surname",example = "Vladislavovich")
    private String surName;
    @Schema(description = "email",example = "ooovladislavchik@gmail.com")
    @Column(name = "email", nullable = false,unique = true)
    private String email;
    @Schema(description = "Url for image",example = "http://...")
    @Column(name = "imageUrl",nullable = false)
    private String imageUrl;
    @Schema(description = "Status",example = "online")
    @Column(name = "status",nullable = false)
    private String status;
    @Temporal(TemporalType.TIMESTAMP)
    @Schema(description = "Registration date")// Для хранения даты и времени
    @Column(name = "registrationDate")
    private Date registrationDate;
    @Column(name = "birthDate")
    @Schema(description = "birthday")
    private Date birthDate;
    @Column(name = "role")
    @Schema(description = "User role",example = "ROLE_ADMIN")
    private String role;

    public Users(long id, String name, String surName, String email, String imageUrl, String status) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.email = email;
        this.imageUrl = imageUrl;
        this.status = status;
    }
}
