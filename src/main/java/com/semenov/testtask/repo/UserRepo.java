package com.semenov.testtask.repo;

import com.semenov.testtask.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<Users,Long> {
    Optional<Users> findById(Long id);

    Optional<Users> findByEmail(String email);
}
