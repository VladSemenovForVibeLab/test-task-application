package com.semenov.testtask.service;

import com.semenov.testtask.model.Users;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface UserService {
    ResponseEntity<Object> save(Users user);
    ResponseEntity<Object> findById(long id);

   ResponseEntity<Object> updateUser(long id);


}
