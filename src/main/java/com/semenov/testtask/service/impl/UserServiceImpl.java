package com.semenov.testtask.service.impl;

import com.semenov.testtask.exception.UserExistingEmailException;
import com.semenov.testtask.exception.UserNotFoundException;
import com.semenov.testtask.model.Users;
import com.semenov.testtask.repo.UserRepo;
import com.semenov.testtask.response.ResponseHandler;
import com.semenov.testtask.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    UserRepo userRepo;
    @Override
    @Transactional
    public ResponseEntity<Object> save(Users user) {
        Optional<Users> existingUserOptional = userRepo.findByEmail(user.getEmail());

        if (existingUserOptional.isPresent() && user.getEmail().equals(existingUserOptional.get().getEmail())) {
            throw new UserExistingEmailException("There is a user with the same email");
        }
        long id = userRepo.save(user).getId();
        return ResponseHandler.responseBuilder("user was saved", HttpStatus.OK,id);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<Object> findById(long id) {
        if(!userRepo.findById(id).isPresent()){
            throw new UserNotFoundException("User does not exist");
        }
        return ResponseHandler.responseBuilder("searched user",HttpStatus.OK,userRepo.findById(id).get());
    }

    @Override
    @Transactional
    public ResponseEntity<Object> updateUser(long id) {
        if(!userRepo.findById(id).isPresent()){
            throw new UserNotFoundException("User does not exist");
        }
        Users users = userRepo.findById(id).get();
        Map<String,String> updateResponse = new HashMap<>();
        updateResponse.put("id",""+users.getId());
        updateResponse.put("previous status", users.getStatus());
        if(users.getStatus().equalsIgnoreCase("offline")){
            users.setStatus("online");
            updateResponse.put("current status",users.getStatus());
        }else {
            users.setStatus("offline");
            updateResponse.put("current status",users.getStatus());
        }
        userRepo.save(users);
        return ResponseHandler.responseBuilder("user was updated",HttpStatus.OK,updateResponse);
    }
}
